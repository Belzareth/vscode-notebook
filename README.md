# Basic VS Code Notes Project
This repo can be cloned and used as a base to easily manage a markdown
"notebook" using Visual Studio Code. It has a built-in Python tool to allow
updating the document tree to allow easily navigating the notebook while hosted
in Git remotes on sites like GitLab and GitHub. Using Git for taking notes also
has the added advantage of getting all notes in source control, easily being
able to go back and see old versions if something isn't quire right anymore.

<!-- This 'Table of Contents' will automatically be updated to contain all created
markdown documents -->

# Table of Contents

# Setup
Setting up this notebook will of course start with cloning the base project. The
following instruction is based on MacOS, but can be easily modified for other
operating systems.

## Requirements
* Python v3.6 installed
  * The default ['update_git.sh'](Resources/update_git.sh) assumes you are using
  a `~/.bash_profile` with a virtualenvwrapper virtual environment called
  `notes`
* [`gitignore_parser'][gitignore_parser] installed into your Python environment

## Adding Keybindings
When first setting up a notebook the following shortcut can be added to the
'keybindings.json' file in your VS Code settings to enable easy-updating of
changes.

```json
{
    "key": "cmd+shift+s",
    "command": "workbench.action.tasks.runTask",
    "ags": "UpdateGit"
}
```

> **Note:** If using a different operating system than MacOS update the 'cmd'
> int the above "key" to the appropriate value

## Adding Notes and Directories
You can easily create grouped notes by adding directories to the root (and even
sub-directories within those directories as deep as you want). The TOC Updater
will automatically build your table of contents by going through each directory
and adding links to all contained markdown files.

> **Note:** Any directories that you wish to include in the notebook tree must
> be initialized with a `README.md` file. You may choose to leave this file
> blank (it will automatically get its own TOC), or fill it with whatever you
> like.

For example you could have:
```
.
├── Awesome.md
├── README.md
├── Programming
│   ├── Python is Awesome.md
│   └── README.md
├── Cool Stuff
│   └── Really Cool Stuff.md
└── Recipes
    ├── README.md
    ├── Breakfast
    │   ├── README.md
    │   └── Pancakes.md
    └── Dinner
        ├── README.md
        └── Pasta.md
```

This will automatically create a Table of Contents in the root `README.md` of:
```md
# Table of Contents
* [Awesome](Awesome.md "Awesome")
* [Programming/](Programming/README.md "Programming Directory")
  * [Python is Awesome](Programming/Python%20is%20Awesome.md "Python is Awesome")
* [Recipes/](Recipes/README.md "Recipes Directory")
  * [Breakfast/](Recipes/Breakfast/README.md "Breakfast Directory")
    * [Pancakes](Recipes/Breakfast/Pancakes.md "Pancakes")
  * [Dinner/](Recipes/Dinner/README.md "Dinner Directory")
    * [Pasta](Recipes/Dinner/Pasta.md "Pasta")
```

> **Note:** The `Cool Stuff` directory was not included since it did not have
> a `README.md` file. Directories without READMEs are not included to allow
> easily embedding reference directories. You can still link to files within
> these directories from other Markdown files.

# Other Resources
If using GitLab make sure to look at the [GitLab Markdown Guide][GL_MD_Guide],
or if using GitHub look at the [GitHub Markdown Guide][GH_MD_Guide].


<!-- Links -->
[gitignore_parser]: https://pypi.org/project/gitignore-parser/
[GL_MD_Guide]: https://gitlab.com/gitlab-org/gitlab/blob/master/doc/user/markdown.md
[GH_MD_Guide]: https://docs.github.com/en/github/writing-on-github/basic-writing-and-formatting-syntax
