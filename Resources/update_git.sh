#!/bin/bash

echo "Updating TOCs"
source ~/.bash_profile
workon notes
python Resources/update_readmes.py

echo "Syncing Notes"
git pull

echo "\nStaging changed files"
git add .

echo "\nCommiting staged files"
git commit -m "Syncing Notes - $(date)"

echo "\nPushing Changes"
git push

echo "\nDone"
