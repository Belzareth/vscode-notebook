import logging
import pathlib
from os import listdir
from os.path import basename, exists, isdir, join
from typing import Callable, List, Tuple
from urllib.parse import quote

from gitignore_parser import parse_gitignore

NOTES_PATH = pathlib.Path().parent.absolute()

TOC_HEADER = "# Table of Contents"

LOG = logging.getLogger("TOC_Updater")
# logging.basicConfig(level=logging.DEBUG)


class VSCodeNotesTOCUpdater:
    def __init__(self):
        self._check_gitignore = parse_gitignore(join(NOTES_PATH, ".gitignore"))
        self.notes_tree = DirTree(NOTES_PATH, self._check_gitignore)

    def update_all_toc(self) -> None:
        self.notes_tree.update_readmes()


class DirTree:
    def __init__(self, root_dir: str, check_gitignore: Callable):
        LOG.debug(f"DirTree init ({root_dir})")
        self._check_gitignore = check_gitignore
        self._files: List[str] = []
        self._dirs: List[DirTree] = []
        self._root_dir = root_dir
        self._build_tree()

    def __bool__(self) -> bool:
        return bool(self._files) or bool(self._dirs)

    def __repr__(self) -> str:
        return (
            f'DirTree("{self._root_dir}", '
            f'parse_gitignore(join("{NOTES_PATH}", ".gitignore")'
        )

    @property
    def root_dir(self):
        return basename(self._root_dir)

    def get_file_with_path(self, filename: str) -> str:
        if filename not in self._files:
            raise FileNotFoundError(
                f"{filename} not fount in this directory. "
                f"Contents are {self._files}"
            )
        return join(self._root_dir, filename)

    def _build_tree(self):
        LOG.debug("Building Tree")
        for item in listdir(self._root_dir):
            item_with_path = join(self._root_dir, item)
            LOG.debug(f" Full path '{item_with_path}'")
            if self._check_gitignore(item_with_path):
                LOG.debug("  Is ignored")
            else:
                if isdir(item_with_path) and _dir_has_readme(item_with_path):
                    LOG.debug("  Is valid dir")
                    dir_tree = DirTree(item_with_path, self._check_gitignore)
                    if dir_tree:
                        self._dirs.append(dir_tree)
                elif item[-3:] == ".md":
                    LOG.debug("  Is valid file")
                    self._files.append(item)
                else:
                    LOG.debug(f"Unknown file item {item}")
        LOG.debug(self._files)
        self._files.sort()
        LOG.debug(self._files)
        LOG.debug(self._dirs)
        self._dirs.sort(key=lambda d: d.root_dir)
        LOG.debug(self._dirs)

    def get_tree(self, indent: int = 0) -> str:
        tree_str = ""
        for filename in self._files:
            tree_str += _indent(indent) + filename + "\n"
        for dirobj in self._dirs:
            tree_str += _indent(indent) + dirobj.root_dir + "\n"
            tree_str += dirobj.get_tree(indent + 1)
        return tree_str

    def get_markdown_links(
        self, indent: int = 0, prev_dir_path: str = ""
    ) -> str:
        LOG.debug("getting markdown links")
        links_str = ""
        for filename in self._files:
            if filename == "README.md":
                pass
            else:
                LOG.debug(f" for file {filename}")
                links_str += (
                    _bullet_indent(indent)
                    + self._gen_markdown_file_link(filename, prev_dir_path)
                    + "\n"
                )
        for dir_obj in self._dirs:
            LOG.debug(f" for dir {prev_dir_path}/{dir_obj.root_dir}")
            links_str += (
                _bullet_indent(indent)
                + self._gen_markdown_dir_link(dir_obj.root_dir, prev_dir_path)
                + "\n"
            )
            links_str += dir_obj.get_markdown_links(
                indent + 1, join(prev_dir_path, dir_obj.root_dir)
            )
        return links_str

    def _gen_markdown_link(self, indent: int, name: str, path: str) -> str:
        markdown_link = _bullet_indent(indent)
        if name[:-3] == ".md":
            markdown_link += self._gen_markdown_file_link(name, path)
        else:
            markdown_link += self._gen_markdown_dir_link(name, path)

    def _gen_markdown_file_link(self, name: str, path: str) -> str:
        title = name.replace(".md", "")
        if path != "":
            path = join(path, name)
        else:
            path = name
        return f'[{title}]({_make_path_linkable(path)} "{title}")'

    def _gen_markdown_dir_link(self, name: str, path: str) -> str:
        if path != "":
            path = join(path, name, "README.md")
        else:
            path = join(name, "README.md")
        return f'[{name}/]({_make_path_linkable(path)} "{name} Directory")'

    def update_readmes(self) -> None:
        try:
            self._update_readme()
        except FileNotFoundError:
            pass
        for directory in self._dirs:
            directory.update_readmes()

    def _update_readme(self) -> None:
        (pre_toc, post_toc) = self._get_readme_contents()
        toc = TOC_HEADER + "\n" + self.get_markdown_links()
        self._set_readme_contents(pre_toc, toc, post_toc)

    def _get_readme_contents(self) -> Tuple[str, str]:
        states = ("pre_toc", "in_toc", "post_toc")
        pre_toc = ""
        post_toc = ""
        current_state = states[0]
        with open(self.get_file_with_path("README.md"), "r") as f:
            for line in f.readlines():
                if (
                    current_state == states[0]
                    and line.strip()
                    and line.strip() == TOC_HEADER
                ):
                    current_state = states[1]
                elif current_state == states[1] and (
                    line.strip() == "" or line.strip()[0] != "*"
                ):
                    current_state = states[2]

                if current_state == states[0]:
                    pre_toc += line
                elif current_state == states[2]:
                    post_toc += line
        return (pre_toc, post_toc)

    def _set_readme_contents(
        self, pre_toc: str, toc: str, post_toc: str
    ) -> None:
        with open(self.get_file_with_path("README.md"), "w") as f:
            f.write(pre_toc + toc + post_toc)


def main() -> None:
    updater = VSCodeNotesTOCUpdater()
    updater.update_all_toc()


def _make_path_linkable(filename: str) -> str:
    """Clean up a filename and make it work for markdown links"""
    return quote(filename, safe="/")


def _dir_has_readme(full_dirpath: str) -> bool:
    return exists(join(full_dirpath, "README.md"))


def _indent(level: int) -> str:
    return "  " * level


def _bullet_indent(level: int) -> str:
    return "  " * level + "* "


if __name__ == "__main__":
    main()
